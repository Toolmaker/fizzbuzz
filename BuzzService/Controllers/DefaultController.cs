﻿using Microsoft.AspNetCore.Mvc;

namespace BuzzService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DefaultController : ControllerBase
    {
        [HttpGet]
        public string Index(int value)
        {
            if (value > 0 && value % 5 == 0)
                return "Buzz";

            return "nope";
        }
    }
}