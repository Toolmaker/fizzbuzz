﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using RestSharp;

namespace FizzBuzzService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        private readonly RestClientFactory _restClientFactory;

        public ServiceController(RestClientFactory restClientFactory)
        {
            _restClientFactory = restClientFactory;
        }

        [HttpGet]
        [Route("Status")]
        public string Status()
        {
            return "We're up and running!";
        }

        [HttpGet]
        [Route("Index")]
        [Route("/")]
        public async Task<string> Index(int value)
        {
            var fizz = await GetFizz(value);
            var buzz = await GetBuzz(value);

            return String.Concat(fizz, buzz);
        }

        private async Task<string> GetFizz(int value)
        {
            var client = _restClientFactory.Create("http://localhost:8000/");
            var request = new RestRequest("fizz", Method.GET);
            request.AddQueryParameter("value", value.ToString());

            var result = await client.ExecuteTaskAsync<string>(request);
            return result.Data;
        }

        private async Task<string> GetBuzz(int value)
        {
            var client = _restClientFactory.Create("http://localhost:8000/");
            var request = new RestRequest("buzz", Method.GET);
            request.AddQueryParameter("value", value.ToString());

            var result = await client.ExecuteTaskAsync<string>(request);
            return result.Data;
        }
    }
}
