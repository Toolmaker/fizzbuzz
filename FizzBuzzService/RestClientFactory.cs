﻿using RestSharp;

namespace FizzBuzzService
{
    public class RestClientFactory
    {
        public IRestClient Create(string url)
        {
            return new RestClient(url)
                .UseSerializer(() => new JsonNetSerializer());
        }
    }
}
