﻿using Microsoft.AspNetCore.Mvc;

namespace FizzService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DefaultController : ControllerBase
    {
        [HttpGet]
        public string Index(int value)
        {
            if (value > 0 && value % 3 == 0)
                return "Fizz";

            return "nope";
        }
    }
}